# !/usr/bin/python3
# _*_ coding: utf-8 _*_
import sys
from PyQt5 import QtWidgets
import daily_face

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    application = daily_face.DailyFace()
    application.show()
    sys.exit(app.exec())
