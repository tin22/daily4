import logging
import os
import sys
from datetime import datetime

import psycopg2
import requests


class DataModule:
    """ Класс для доступа к данным через API
    """

    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.API_URL = 'http://lifedesc/'

    def get_day(self, desired_date: str) -> dict:
        """ Получить из базы объект 'день'.
            Из базы выбираются данные за desired_date. Если данных нет, и desired_date соответствует
            текущей дате, то создается 'пустой день' с этой датой. В любой другой день, если нет данных,
            программа прекращает работу с соответсвующим сообщением в лог-файле.
            INPUT: desired_date <str> - Дата за которую требуется получить данные;
            OUTPUT: <dict> - Словарь данных за указанную дату.
        """

        # Вместо реальных данных сформировать и возвратить словарь идентичный настоящему объекту-дню
        # daily = {
        #     'id': 192,
        #     'date': '2020-07-17',
        #     'weight': '78.6',
        #     'bmi': '22.97',
        #     'push_ups': 20,
        #     'squates': 10,
        #     'steps': 22664,
        #     'sleeptime': '07:42:00',
        #     'deep_sleeptime': '01:55:00',
        #     'temperature': '36.3',
        #     'thoughts': [{
        #         'daily': 192,
        #         'timestamp': '2020-07-17T19:53:03',
        #         'record': 'Сегодня делал укол в левую ногу. Есть ощущение, что ноге после укола стало лучше.',
        #         'tags': [{'id': 4, 'tag': 'Здоровье'}, {'id': 11, 'tag': 'Лекарства'}]
        #                 },
        #                 {
        #         'daily': 192,
        #         'timestamp': '2020-07-17T07:22:30',
        #         'record': 'Почитал статью Object-Oriented Programming (OOP) in Python 3. Ощутимо [заболела] голова, значительно ухудшились ощущения в ноге. Выпил витаминку и пирацетам. Улучшений не заметил.',
        #         'tags': [{'id': 4, 'tag': 'Здоровье'}, {'id': 11, 'tag': 'Лекарства'}]
        #                 },
        #                 {
        #         'daily': 192,
        #         'timestamp': '2020-07-17T04:02:33',
        #         'record': 'Спал, вроде, хорошо. Самочувствие примерно на среднем для последнего времени уровне. Вес несколько удивил: я ожидал, что будет меньше, чем вчера, а оказалось больше и порядочно.',
        #         'tags': [{'id': 4, 'tag': 'Здоровье'}]
        #                 }]
        # }
        # return daily

        url = self.API_URL + 'daily/' + desired_date + '/'
        res = requests.get(url)

        print(url, res.status_code)
        if res.status_code == 404 and \
                desired_date == datetime.today().strftime("%Y-%m-%d"):  # Данных нет и desired_date сегодня

            # Создаем новый день
            new_day = {
                'date': desired_date,
                'push_ups': 0,
                'squates': 0,
                'weight': 0.0,
                'bmi': 0.0,
                'steps': 0,
                'deep_sleeptime': '00:00:00',
                'sleeptime': '00:00:00',
                'temperature': '35.0',
                'thoughts': []
            }
            try:
                r = requests.post('http://lifedesc/last/', new_day, auth=('admin', '123'))
                if r.status_code == 201:
                    self.logger.info('New day created. Status code = 201. Payload: ' + str(new_day))
                else:
                    self.logger.exception('Something went wrong.')
            except:
                self.logger.exception('Something went wrong.')
                sys.exit()
            return new_day

        if res.status_code == 200:
            self.logger.info('Data received.')
            return res.json()

        # desired_date не сегодня и данных за desired_date в базе нет
        self.logger.exception('Нет данных. Аварийное завершение. Status code = ' + str(res.status_code))
        sys.exit()

    def get_thoughts(self, desired_date: str) -> dict:
        """ Получить из базы мысли дня.
            INPUT: desired_date <str> - Интересующая дата;
            OUTPUT: <dict> - словарь с мыслями.
        """
        pass

    def get_all_tags(self):
        """ Получить [пока] все возможные тэги
        """
        tags = {}
        url = self.API_URL + 'alltags/'
        res = requests.get(url)
        for tag in res.json():
            tags[tag['tag']] = tag['id']
        return tags

    def get_last_days(self) -> list:
        url = self.API_URL + 'last/'
        res = requests.get(url).json()
        return res

    def put_day(self, desired_date: str, new_day: dict) -> bool:
        """ Поместить объект 'день' в базу.
            INPUT: new_day <dict> - словарь с данными;
            OUTPUT: <bool> - True, если данные успешно записаны.
        """
        url = self.API_URL + 'daily/' + desired_date + '/'
        res = requests.put(url, json=new_day)
        if res.status_code == 200:
            self.logger.info('Data sent successfully. Status code is ' + str(res.status_code) +
                             '.' + 'Data: ' + str(new_day))
            return True
        else:
            self.logger.error('Something went wrong. Status code is ' + str(res.status_code) + '.')
            return False


