#!/usr/bin/python3
# -*- coding: utf-8 -*-
""" Построить графики продолжительности сна
"""
import matplotlib.pyplot as plt
# from datetime import datetime
import psycopg2

with psycopg2.connect(host='localhost', port='5432',
                      database='lifedesc', user='ldw',
                      password='123') as conn:
    cur = conn.cursor()
    cur.execute(
        "SELECT date, sleeptime, deep_sleeptime FROM daily WHERE date "
        "BETWEEN (CURRENT_DATE - integer '%s') AND CURRENT_DATE ORDER BY date;", (14,))

x, y1, y2 = [], [], []
for row in cur:
    x.append(row[0])
    y1.append(row[1].seconds/60/60)  # sleeptime
    y2.append(row[2].seconds/60/60)  # deep_sleeprime

ax1 = plt.subplot(1, 1, 1)
ax1.set_title('Сон', fontsize=10)
plt.grid(True)
ax1.bar(x, y1, color='aqua')
ax1.bar(x, y2, color='blue')
ax1.set_xlabel('Дата', fontsize=8)
ax1.set_ylabel('Общий/глубокий', fontsize=8)
plt.setp(ax1.get_xticklabels(), rotation=30, ha='right', fontsize=8)
plt.setp(ax1.get_yticklabels(), fontsize=8)
plt.tight_layout()
plt.gcf().canvas.set_window_title('Продолжительность сна')

plt.show()
